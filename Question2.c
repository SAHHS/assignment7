//Write a program to find the Frequency of a given character in a given string.
#include <stdio.h>
int main()
{
    char str[1000], ch;
    printf("Enter a sentence: ");
    fgets(str, sizeof(str), stdin);
    printf("What is the character do you want to find the frequency?: ");
    scanf("%c", &ch);

    int i,count=0;

    for (int i = 0; str[i] != '\0'; ++i) {
        if (ch == str[i])
            ++count;
    }

    printf("Frequency of %c = %d", ch, count);
    return 0;
}

